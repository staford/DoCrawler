<?php

/**
 * @author 暮雨秋晨
 * @copyright 2014
 */

/**
 * 变量信息打印函数
 */
function dump()
{
    $args = func_get_args();
    if (!empty($args)) {
        foreach ($args as $arg) {
            echo ("\r\n" . '<br />--[Debug start]--<br />' . "\r\n");
            if (is_array($arg)) {
                echo ('<pre>');
                print_r($arg);
                echo ('</pre>');
            } elseif (is_string($arg)) {
                echo ($arg);
            } else {
                var_dump($arg);
            }
            echo ("\r\n" . '<br />--[Debug   end]--<br />' . "\r\n");
        }
    }
}

function save2file($file, $res)
{
    $fp = fopen($file, 'w', false);
    if (flock($fp, LOCK_EX)) {
        fwrite($fp, $res);
        flock($fp, LOCK_UN);
        fclose($fp);
        return true;
    } else {
        fclose($fp);
        return false;
    }
}

function autoload($name)
{
    $name = strtolower($name);
    if (is_file($file = 'libs/' . $name . '.php')) {
        require_once $file;
    }
}

spl_autoload_register('autoload');
